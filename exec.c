#include <linux/kernel.h>
#include <linux/module.h>

int init_module(void) {
  char *argv[] = {"/bin/sh", "-c", "echo TEST > /lib64/TEST", 0};
  printk(KERN_INFO "Starting ...\n");
  return call_usermodehelper(argv[0], argv, 0, UMH_WAIT_PROC);
  printk(KERN_INFO "... done\n");
  return 0;
}

void cleanup_module(void) {}

MODULE_LICENSE("GPL");
